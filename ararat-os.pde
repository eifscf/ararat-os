//OSTYPE (formely KS OS for Microcontrollers)
//Version OSVERSION
//Adapted for use in the RTPII-mjuA truck
//(c) KS Research, 2012

#include <Servo.h>
#include <Wire.h>
#include <EEPROM.h>
#include <OneWire.h>
#include <LiquidCrystal.h>

#define DS18S20_ID 0x10
#define DS18B20_ID 0x28
//#define EEEPROMADDR 0x50
#define LEFT_IRS_PIN 2 //A
#define RIGHT_IRS_PIN 1 //A
#define PRECISE_IRS_PIN 0 //A
#define AUTHPIN 4
#define DSPIN 3
#define TORCHPIN 7
#define RBPIN 2
//#define SERVOB 96
#define HALLPIN 3 //A
#define DS1307ADDR 0x68
#define GDBVAL 20
#define OSVERSION "v.0.5-bPI20"
#define OSTYPE "ARARAT:OS"
#define DEBUGVERSION "DEBUG-16"
#define RBVERSION "ARRB 11"
#define ARSH_MOTD "arsh v.22.1.2 by KSR/WWAPS"
#define TSDELAY 8000
#define LWDELAY 30000
#define BDRATE 115200

#define LIRRA 740
#define RIRRA 745

OneWire dts(DSPIN);
Servo leftservo; //left servo on pin 9, right - on pin 10
Servo rightservo;
LiquidCrystal lcd(5, 6, 8, 11, 12, 13); //RS - 5, Enable - 6, DB4 - 8, DB5 - 11, DB6 - 12, DB7 - 13


int leftservob = 91;
int rightservob = 92;
int day = 0;
int dow = 0;
int month = 0;
int year = 0;
int hour = 0;
int minute = 0;
int second = 0;
//unsigned int lasteaddr = 0;
float hall = 0;
int lirs = 0;
int pirs = 0;
int rirs = 0;
float temp = 0;
int lastmil1;
int lastmil2;
byte userrow;
byte usercol;
int memaddr;
int loglast;

byte key[8] = {
  B01110,
  B10001,
  B01110,
  B00100,
  B00111,
  B00100,
  B00111,
};

byte degree[8] = {
  B01110,
  B10001,
  B01110,
  B00000,
  B00000,
  B00000,
  B00000,
};

int FreeRam () {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

void PinParse() {
  pinMode(TORCHPIN, OUTPUT);
  //pinMode(AUTHPIN, OUTPUT);
  //digitalWrite(AUTHPIN, 1);
  //delay(20);
  //digitalWrite(AUTHPIN, 0);
  pinMode(AUTHPIN, INPUT);
}

void PerformAuth() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.write(0);
  lcd.print("=X");
  delay(1000);
  lcd.setCursor(0, 1);
  lcd.print("KEY?");
  lcd.setCursor(2, 0);
  delay(1000);
  Serial.print(FreeRam());
  Serial.println("  B FREE");
  Dprompt();
  AuthP();
  Serial.println("CYCLESTART");
  do
  {
    AuthP();
    Serial.print("INCYCLE:us");
    Serial.println(millis());
    delay(10);
  } while(!digitalRead(AUTHPIN));
  Dprompt();
  AuthP();
  Serial.println("CYCLEEND");
  Serial.print(FreeRam());
  Serial.println("  B FREE");
  delay(1000);
  lcd.print("V");
  lcd.setCursor(0,1);
  lcd.print("AUTH SUCESSFUL");
  Dprompt();
  Serial.println("AUTH:SUCESSFUL");
  delay(20);
  lcd.clear();
  delay(10);
}

float IfIRS(int irspin) {
  int irb = analogRead(irspin);
  irb = map(irb, 0, 1023, 0, 5000);
  irb = irb / 6;
  return irb;
}

void IfHall() {
  int raw = analogRead(HALLPIN);
  boolean minus = false;
  float voltage = raw / 204;
  if(voltage < 2.5) {
    minus = true;
  } else if (voltage > 2.5) {
    minus = false;
  }
  float gauss = voltage / 0.0031;
  hall = gauss / 10000;
}

void IfTruckControl(int dir, int cspd) { //direction codes - 1 = left, 2 = right, 3 = forward, 4 = backward, 0 = stop
  switch (dir) {
    case 1:
      rightservo.write(rightservob);
      leftservo.write(leftservob + cspd);
      Dprompt();
      ITCprompt();
      Serial.println("LEFT");
      break;
    case 2:
      leftservo.write(leftservob);
      rightservo.write(rightservob - cspd);
      Dprompt();
      ITCprompt();
      Serial.println("RIGHT");
      break;
    case 3:
      leftservo.write(leftservob - cspd);
      rightservo.write(rightservob + cspd);
      Dprompt();
      ITCprompt();
      Serial.println("FW");
      break;
    case 4:
      leftservo.write(leftservob + cspd);
      rightservo.write(rightservob - cspd);
      Dprompt();
      ITCprompt();
      Serial.println("BW");
      break;
    case 0:
      leftservo.write(leftservob);
      rightservo.write(rightservob);
      Dprompt();
      ITCprompt();
      Serial.println("STOP");
      break;
  }
}

byte IfLogRead() {
  int raw = IfIRS(PRECISE_IRS_PIN);
  int mapped = map(raw, -80, 80, 0, 80);
  mapped = mapped / 4;
  return byte(mapped);
}

/*boolean IfDebounce(int dpin) {
  boolean dbv1 = digitalRead(dpin);
  delay(GDBVAL);
  boolean dbv2 = digitalRead(dpin);
  if(dbv2 != dbv1) {
    return false;
  } else {
    return true;
  }
}*/

void IfLogRecord() {
  byte recs[7];
  lcd.clear();
  lcd.setCursor(2, 0);
  lcd.print("SCANNING...");
  lcd.setCursor(5, 1);
  lcd.print("[");
  lcd.setCursor(11, 1);
  lcd.print("]");
  lcd.setCursor(6, 1);
  Dprompt();
  Serial.println("LOGRECORD");
  if(memaddr >= 1024) {
    Dprompt();
    Serial.println("CLEEPROM");
    for (int i = 0; i < 1024; i++) EEPROM.write(i, 0);
    memaddr = 0;
  }
  //IfTruckControl(1, 80);
  //delay(3000);
  IfTruckControl(0, 0);
  recs[0] = IfLogRead();
  IfTruckControl(2, 80);
  for (int i = 1; i < 5/*6*/; i++)
  {
    if(memaddr >= 1024) {
      Dprompt();
      Serial.println("CLEEPROM");
      for (int i = 0; i < 1024; i++) EEPROM.write(i, 0);
    }
    delay(1000);
    recs[i] = IfLogRead();
    Dprompt();
    Serial.print("LOGRECORD:");
    Serial.print(i, DEC);
    Serial.print(":");
    Serial.println(recs[i], DEC);
    lcd.print("#");
  }
  UpdateTimestamp();
  recs[5] = hour;
  recs[6] = minute;
  recs[7] = second;
  //IfTruckControl(1, 80);
  //delay(6000);
  IfTruckControl(0, 0);
  for (int i = 0; i < 8; i++) {
    EEPROM.write(memaddr, recs[i]);
    memaddr++;
  }
  delay(150);
  lcd.clear();
}

void IfThermo() {
  float _temp;
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
 
  //find a device
  if (!dts.search(addr)) {
    dts.reset_search();
    delay(1);
  }
  if (OneWire::crc8( addr, 7) != addr[7]) {
    delay(1);
  }
  if (addr[0] != DS18S20_ID && addr[0] != DS18B20_ID) {
    delay(1);
  }
  dts.reset();
  dts.select(addr);
  // Start conversion
  dts.write(0x44, 1);
  // Wait some time...
  delay(850);
  present = dts.reset();
  dts.select(addr);
  // Issue Read scratchpad command
  dts.write(0xBE);
  // Receive 9 bytes
  for ( i = 0; i < 9; i++) {
    data[i] = dts.read();
  }
  // Calculate temperature value
  _temp = ( (data[1] << 8) + data[0] )*0.0625;
  temp = _temp;
}
/*
void MEM_Wbyte(unsigned int eeaddress, byte data ) {
  int rdata = data;
  Wire.beginTransmission(EEEPROMADDR);
  Wire.send((int)(eeaddress >> 8)); // MSB
  Wire.send((int)(eeaddress & 0xFF)); // LSB
  Wire.send(rdata);
  Wire.endTransmission();
}

byte MEM_Rbyte(unsigned int eeaddress ) {
  byte rdata = 0xFF;
  Wire.beginTransmission(EEEPROMADDR);
  Wire.send((int)(eeaddress >> 8)); // MSB
  Wire.send((int)(eeaddress & 0xFF)); // LSB
  Wire.endTransmission();
  Wire.requestFrom(EEEPROMADDR,1);
  if (Wire.available()) rdata = Wire.receive();
    return rdata;
}*/

  // maybe let's not read more than 30 or 32 bytes at a time!
//void MEM_Rarr(unsigned int eeaddress, byte *buffer, int length ) {
  //Wire.beginTransmission(EEEPROMADDR);
  //Wire.send((int)(eeaddress >> 8)); // MSB
  //Wire.send((int)(eeaddress & 0xFF)); // LSB
  //Wire.endTransmission();
  //Wire.requestFrom(EEEPROMADDR,length);
  //int c = 0;
  //for ( c = 0; c < length; c++ )
  //    if (Wire.available()) buffer[c] = Wire.receive();
//}
/*byte bcdToDec(byte val)
{
  return ( (val/16*10) + (val%16) );
}

byte decToBcd(byte val)
{
  return ( (val/10*16) + (val%10) );
}*/
 

/*void RTCRead() {
  Serial.println("DEBUG:RTC:BEGIN");
  Wire.beginTransmission(DS1307ADDR);   // Open I2C line in write mode
  Wire.send(0x00);                              // Set the register pointer to (0x00)
  Serial.println("DEBUG:RTC:SET");
  Wire.endTransmission();                       // End Write Transmission 
  Serial.println("DEBUG:RTC:REQUEST");
 
  Wire.requestFrom(DS1307ADDR, 7);      // Open the I2C line in send mode
 
  second     = bcdToDec(Wire.receive() & 0x7f); // Read seven bytes of data
  minute     = bcdToDec(Wire.receive());
  hour       = bcdToDec(Wire.receive() & 0x3f);  
  dow        = bcdToDec(Wire.receive());
  day        = bcdToDec(Wire.receive());
  month      = bcdToDec(Wire.receive());
  year       = bcdToDec(Wire.receive());
}

void RTCWrite() {
  Wire.beginTransmission(DS1307ADDR);  // Open I2C line in write mode
 
  Wire.send(0x00);                           // Set the register pointer to (0x00)
  Wire.send(decToBcd(second));               // Write seven bytes
  Wire.send(decToBcd(minute));
  Wire.send(decToBcd(hour));      
  Wire.send(decToBcd(dow));
  Wire.send(decToBcd(day));
  Wire.send(decToBcd(month));
  Wire.send(decToBcd(year));
  Wire.endTransmission();                    // End write mode
}*/
  
/*void MemWrEvent() {//Format: 0x00 - hour, 0x01 - minute, 0x02 - second, 0x03 - lirs, 0x04 - rirs, 0x05 - pirs, 0x06 - lservo, 0x07 - rservo
   Serial.println("DEBUG:WREVENT");
   if (lasteaddr >= 32767) {
     Serial.println("DEBUG:WREVENT:RETURN");
     return;
   }
   Serial.println("DEBUG:WREVENT:UPDATE");
   RTCRead();
   IfIRSCy();
   lasteaddr++;
   Serial.println("DEBUG:WREVENT:HOUR");
   MEM_Wbyte(lasteaddr, byte(hour));
   lasteaddr++;
   Serial.println("DEBUG:WREVENT:MINUTE");
   MEM_Wbyte(lasteaddr, byte(minute));
   lasteaddr++;
   Serial.println("DEBUG:WREVENT:SECOND");
   MEM_Wbyte(lasteaddr, byte(second));
   lasteaddr++;
   int tmp = IfIRS(LEFT_IRS_PIN);
   Serial.println("DEBUG:WREVENT:LIRS");
   MEM_Wbyte(lasteaddr, byte(tmp));
   lasteaddr++;
   tmp = IfIRS(RIGHT_IRS_PIN);
   Serial.println("DEBUG:WREVENT:RIRS");
   MEM_Wbyte(lasteaddr, byte(tmp));
   lasteaddr++;
   tmp = IfIRS(PRECISE_IRS_PIN);
   Serial.println("DEBUG:WREVENT:PIRS");
   MEM_Wbyte(lasteaddr, byte(tmp));
   lasteaddr++;
   Serial.println("DEBUG:WREVENT:LS");
   MEM_Wbyte(lasteaddr, byte(lservobalance));
   lasteaddr++;
   Serial.println("DEBUG:WREVENT:RS");
   MEM_Wbyte(lasteaddr, byte(rservobalance)); 
}*/

/*void MemDump() {
  int caddr = 0;
  Serial.println("Memory dump started.");
  int i;
  for(i = 0; i < 4681; i++) {
    Serial.print("DR ST 0x");
    Serial.print(caddr);
    Serial.print(" END 0x");
    Serial.print(caddr + 7);
    Serial.println(" :");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(":");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(".");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(" LIRS:");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(" RIRS:");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(" PIRS:");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(" LS:");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
    Serial.print(" RS:");
    Serial.print(int(MEM_Rbyte(caddr)));
    caddr++;
  }
}*/

void Interpretate() {
  int icmd = 0;
  int acc = 0;
  int ieadd = 0;
  while (icmd != '^') {
    icmd = EEPROM.read(ieadd);
    if (icmd == '1') {
        IfTruckControl(0, 0);
    } else
    if (icmd == '2') {
        IfTruckControl(3, 80);
    } else
    if (icmd == '3') {
        IfTruckControl(4, 80);
    } else
    if (icmd == '4') {
        IfTruckControl(1, 80);
    } else
    if (icmd == '5') {
        IfTruckControl(2, 80);
    } else
    if (icmd == 'd') {
        Serial.println("D250");
        delay(250);
    } else
    if (icmd == 'D') {
        Serial.println("D500");
        delay(500);
    } else
    if (icmd == 'j') {
        Serial.println("JMP");
        ieadd++;
        int jad = EEPROM.read(ieadd);
        ieadd++;
        int mul = EEPROM.read(ieadd);
        if(mul = '2') jad = jad * 2;
        if(mul = '4') jad = jad * 4;
        ieadd = jad;
    } else
    if (icmd == 'i') {
        Serial.println("IF");
        if(acc != 0) ieadd++;
    } else
    if (icmd == '+') {
        acc++;
        Serial.println("A++");
    } else
    if (icmd == '-') {
        acc--;
        Serial.println("A--");
    } else {
        Serial.print("RM INT FAULT AT ");
        Serial.println(ieadd);
    }
    ieadd++;
    if(ieadd > 1024) {
      ieadd = 0;
      Serial.println("+LOOP");
    }
    Serial.println("+I");
  }
}

byte decToBcd(byte val)
{
  return ( (val/10*16) + (val%10) );
}
 
byte bcdToDec(byte val)
{
  return ( (val/16*10) + (val%16) );
}

void UpdateTimestamp()
{
      Wire.beginTransmission(DS1307ADDR); 
      Wire.send((byte)0x00);
      Wire.endTransmission();
      Wire.requestFrom(DS1307ADDR, 7);
      second     = bcdToDec(Wire.receive() & 0x7f);
      minute     = bcdToDec(Wire.receive());
      hour       = bcdToDec(Wire.receive() & 0x3f);  
      dow        = bcdToDec(Wire.receive());
      day        = bcdToDec(Wire.receive());
      month      = bcdToDec(Wire.receive());
      year       = bcdToDec(Wire.receive());
}

void ProgramM()
{
  int curbyte = 0;
  int curadd = 0;
  Serial.println("Writing started at 0x0.");
  while (curbyte != '^') {
    while(Serial.available() < 1) {}
    curbyte = Serial.read();
    if(curbyte == 127) {
      curadd--;
      Serial.println("BCKSP");
    }
    if(curadd > 1024) break;
    if(curbyte != 127) {
      EEPROM.write(curadd, curbyte);
      Serial.print("0x");
      Serial.print(curadd);
      Serial.print(" : ");
      Serial.println(curbyte);
    curadd++;
    }
  }
  Serial.println("Writing completed.");
}

void Prompt()
{
  Serial.println("CMD>");
}

void Ierr()
{
  Serial.println("Input error!");
}

void Rprompt()
{
  Serial.println("R>");
}

void AuthP()
{
  Serial.print("AUTH:");
}

void ITCprompt()
{
  Serial.print("ITC:");
}

void Goodbye()
{
  Serial.println("Goodbye!");
}

void Performed()
{
  Serial.println(" Performed.");
}

void InLoop()
{
  Dprompt();
  Serial.print("LOOP:");
}

void Dprompt()
{
  Serial.print("DEBUG:");
}

int NumInput()
{
  while(Serial.available() <1) {
    delay(1);
  }
  int asciiin = Serial.read();
  switch(asciiin) {
    case '0':
      return 0;
    case '1':
      return 1;
    case '2':
      return 2;
    case '3':
      return 3;
    case '4':
      return 4;
    case '5':
      return 5;
    case '6':
      return 6;
    case '7':
      return 7;
    case '8':
      return 8;
    case '9':
      return 9;
  }
}
    

void Console() {
  boolean exit = false;
  Serial.println(ARSH_MOTD);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("IN CONSOLE MODE");
  Prompt();
  while(exit == false) {
  if (Serial.available() > 0) {
    int cmd = Serial.read();
    Serial.print("CMD: ");
    Serial.println(cmd, DEC);
    
    if (cmd == 'x') {
      Goodbye();
      exit = true;
    }
    
    if (cmd == 'l') {
      PerformAuth();
      Serial.print("AUTH");
      Performed();
    }
      
    if (cmd == 'd') {
      Serial.println("DUMP ST AT 0x00");
      for(int a = 0; a < 1024; a++) {
          Serial.print("R@ ");
          Serial.print(a);
          Serial.print(" [");
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print(" ");
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print(" ");
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print(" ");
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print(" ");
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print("] ");
          
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print(":");
          Serial.print(EEPROM.read(a), DEC);
          a++;
          Serial.print(".");
          Serial.println(EEPROM.read(a), DEC);
      }
      Serial.println("DUMP END");
    }
    
    if (cmd == 'k') {
      IfLogRecord();
    }
    
    if (cmd == 'm') {
      Serial.println("Authenticate please.");
      PerformAuth();
      Dprompt();
      Serial.println("CLEEPROM");
      for (int i = 0; i < 1024; i++) EEPROM.write(i, 0);
      memaddr = 0;
      Serial.println("EEPROM cleared.");
    }
    
    if (cmd == 'h') {
      Serial.println("X, H, ^W, D, ^D, K, M, T, I, U, ^T, ^S, F, R, V, L, W, C, P, ^P, ^R");
    }
    
    if (cmd == 'R') {
      Redboot();
    }
    
    if (cmd == 'W') {
      Serial.println("DUMP ST AT 0x00");
      for(int a = 0; a < 1024; a++) {
          Serial.print("0x");
          Serial.print(a);
          Serial.print(" ");
          Serial.println(EEPROM.read(a), DEC);
      }
      Serial.println("DUMP END");
    }
    
    if (cmd == 'T') {
      UpdateTimestamp();
      Serial.print(dow);
      Serial.print("  ");
      Serial.print(day);
      Serial.print(".");
      Serial.print(month);
      Serial.print(".");
      Serial.print(year);
      Serial.print(" ");
      Serial.print(hour);
      Serial.print(":");
      Serial.print(minute);
      Serial.print(".");
      Serial.println(second);
    }
    
    if (cmd == 'S') {
      int i1;
      int i2;
      Serial.println("Day?");
      i1 = NumInput();
      i2 = NumInput();
      Serial.print(i1);
      Serial.println(i2);
      int uday = i1 * 10 + i2;
      if(uday > 31) {
        Ierr();
        break;
      }
      Serial.println("DOW?");
      i1 = NumInput();
      Serial.print(i1);
      int udow = i1;
      if(udow > 7) {
        Ierr();
        break;
      }
      Serial.println("Month?");
      i1 = NumInput();
      i2 = NumInput();
      Serial.print(i1);
      Serial.println(i2);
      int umonth = i1 * 10 + i2;
      if(umonth > 12) {
        Ierr();
        break;
      }
      Serial.println("Year (YY)?");
      i1 = NumInput();
      i2 = NumInput();
      Serial.print(i1);
      Serial.println(i2);
      int uyear = i1 * 10 + i2;
      Serial.println("Hour (24)?");
      i1 = NumInput();
      i2 = NumInput();
      Serial.print(i1);
      Serial.println(i2);
      int uhour = i1 * 10 + i2;
      if(uhour > 24) {
        Ierr();
        break;
      }
      Serial.println("Minute?");
      i1 = NumInput();
      i2 = NumInput();
      Serial.print(i1);
      Serial.println(i2);
      int uminute = i1 * 10 + i2;
      if(uminute > 59) {
        Ierr();
        break;
      }
      Serial.println("Second?");
      i1 = NumInput();
      i2 = NumInput();
      Serial.print(i1);
      Serial.println(i2);
      int usecond = i1 * 10 + i2;
      if(usecond > 59) {
        Ierr();
        break;
      }
      Wire.beginTransmission(DS1307ADDR); 
      Wire.send((byte)0x00);
      Wire.send(decToBcd(usecond));
      Wire.send(decToBcd(uminute));
      Wire.send(decToBcd(uhour));      
      Wire.send(decToBcd(udow));
      Wire.send(decToBcd(uday));
      Wire.send(decToBcd(umonth));
      Wire.send(decToBcd(uyear));
      Wire.endTransmission();
    }
    
    if (cmd == 'D') {
      for(int i = Serial.available(); i < 1; i--) {
        Serial.read();
      }
      while(Serial.available() < 1) {
        Serial.print("IRSS ");
        Serial.print("L:");
        int lef = IfIRS(LEFT_IRS_PIN);
        Serial.print(lef);
        Serial.print(" R:");
        int rgt = IfIRS(RIGHT_IRS_PIN);
        Serial.print(rgt);
        Serial.print(" P:");
        int prc = IfIRS(PRECISE_IRS_PIN);
        Serial.println(prc);
        Serial.print("AUTHPIN ");
        if(digitalRead(AUTHPIN)) {
          Serial.println("ON");
        } else {
          Serial.println("OFF");
        }
        Serial.print("RBPIN ");
        if(digitalRead(RBPIN)) {
          Serial.println("ON");
        } else {
          Serial.println("OFF");
        }
        delay(1000);
      }
    }
    
    if (cmd == 'P') {
      ProgramM();
    }
    
    if (cmd == 'p') {
      Interpretate();
    }
    
    if (cmd == '!') {
      Dprompt();
      Serial.println("THERITHERE");
    }
    
    if (cmd == 'c') {
      lcd.clear();
      Serial.println("Display cleared.");
    }
    
    if (cmd == 'v') {
      Serial.println(OSTYPE);
      Serial.println(OSVERSION);
    }
    
    if (cmd == 'w') {
      Serial.println("Enter char array: ('^' to break)");
      while(1){
        if (userrow >= 2) {
          lcd.clear();
          userrow = 0;
          Serial.println(" A CLS");
        }
        if (usercol >= 16) {
          usercol = 0;
          userrow++;
          Serial.println(" A CR LF");
        }
        int sym = Serial.read();
        if (sym == '^') break; 
        if (sym != -1) {
          Serial.print(char(sym));
          lcd.setCursor(usercol, userrow);
          lcd.print(char(sym));
          usercol++;
        }
      }
       Serial.println();
    }
        
        
      
    
    if (cmd == 'r') {
      int spd = 97;
      Serial.println("RoCon v.1.1 by KSR");
      Serial.println("R>");
      while(1) {
        if (Serial.available() > 0) {
          int rcmd = Serial.read();
          Serial.print("RCMD: ");
          Serial.println(rcmd, DEC);
          //if (rcmd == 'h') {
          //  Serial.println("eXit, forWard, backward(S), left(A), right(D), sfor^Ward, sbackward(^S), sleft(^A), sright(^D), stop(Q), incRease(speed), dEcrease(speed)");
          //  Serial.println("R>");
          //}
          
          if (rcmd == 'r') {
            spd = spd + 10;
            Serial.print("RSpeed = ");
            Serial.println(spd);
            Rprompt();
          }
          
          if (rcmd == 'z') {
            Serial.print("LEFT? ");
            int ni = NumInput();
            ni = ni + 90;
            Serial.println(ni);
            leftservob = ni;
            ni = 0;
            Serial.print("RIGHT? ");
            ni = NumInput();
            ni = ni + 90;
            Serial.println(ni);
            rightservob = ni;
            Rprompt();
          }
          
          if (rcmd == 'e') {
            spd = spd - 10;
            Serial.print("RSpeed = ");
            Serial.println(spd);
            Rprompt();
          }
          
          if (rcmd == 'q') {
            IfTruckControl(0, spd);
            Serial.print("RSTOP");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'd') {
            IfTruckControl(2, spd);
            Serial.print("RRIGHT");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'a') {
            IfTruckControl(1, spd);
            Serial.print("RLEFT");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'w') {
            IfTruckControl(3, spd);
            Serial.println("RFW");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 's') {
            IfTruckControl(4, spd);
            Serial.println("RBW");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'D') {
            IfTruckControl(2, spd);
            delay(1000);
            IfTruckControl(0, spd);
            Serial.print("sRRIGHT");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'A') {
            IfTruckControl(1, spd);
            delay(1000);
            IfTruckControl(0, spd);
            Serial.print("sRLEFT");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'W') {
            IfTruckControl(3, spd);
            delay(1000);
            IfTruckControl(0, spd);
            Serial.println("sRFW");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'S') {
            IfTruckControl(4, spd);
            delay(1000);
            IfTruckControl(0, spd);
            Serial.println("sRBW");
            Performed();
            Rprompt();
          }
          
          if (rcmd == 'x') {
            Goodbye();
            break;
          }
        }
      }
    }
          
    
    /*if (cmd == 'D') {
      MemDump();
      Prompt();
    }*/
    
    /*if (cmd == 'C') {
      Serial.println("Perform authentication please!");
      PerformAuth();
      Serial.println("Memory clearing started...");
      for(int i = 0; i < 32768; i++) {
        MEM_Wbyte(i, 0);
      }
      lasteaddr = 0;
      Serial.println("Memory clearing completed.");
      Prompt();
    }*/
    
    if (cmd == 't') {
      Serial.print("t = ");
      IfThermo();
      Serial.print(temp);
      Serial.println(" C DEG");
    }
    
    if (cmd == 'u') {
      unsigned long time = millis() / 1000;
      Serial.print("Uptime: ");
      Serial.print(time);
      Serial.println(" S.");
    }
    
    if (cmd == 'f') {
      int free = FreeRam();
      Serial.print(free);
      Serial.println(" bytes free in RAM");
    }
    
    if (cmd == 'i') {
      Serial.print("L:");
      int lef = IfIRS(LEFT_IRS_PIN);
      Serial.print(lef);
      Serial.print(" R:");
      int rgt = IfIRS(RIGHT_IRS_PIN);
      Serial.print(rgt);
      Serial.print(" P:");
      int prc = IfIRS(PRECISE_IRS_PIN);
      Serial.println(prc);
    }
    
    Prompt();
    
    /*if (cmd == 'I') {
      RTCRead();
      Serial.println("Current time is : ");
      Serial.print(day);
      Serial.print(".");
      Serial.print(month);
      Serial.print(".");
      Serial.print(year);
      Serial.print(" ");
      Serial.print(hour);
      Serial.print(":");
      Serial.print(minute);
      Serial.print(".");
      Serial.println(second);
      Prompt();
    }*/
    
   /* if (cmd = 'E') {
       Serial.println("Please enter in format : SSMMHHWDDMMYY");
       second = (byte) ((Serial.read() - 48) * 10 + (Serial.read() - 48)); // Use of (byte) type casting and ascii math to achieve result.  
       minute = (byte) ((Serial.read() - 48) *10 +  (Serial.read() - 48));
       hour  = (byte) ((Serial.read() - 48) *10 +  (Serial.read() - 48));
       dow = (byte) (Serial.read() - 48);
       day = (byte) ((Serial.read() - 48) *10 +  (Serial.read() - 48));
       month = (byte) ((Serial.read() - 48) *10 +  (Serial.read() - 48));
       year= (byte) ((Serial.read() - 48) *10 +  (Serial.read() - 48));
       Wire.beginTransmission(DS1307ADDR);
       Wire.send(0x00);
       Wire.send(decToBcd(second));    // 0 to bit 7 starts the clock
       Wire.send(decToBcd(minute));
       Wire.send(decToBcd(hour));      // If you want 12 hour am/pm you need to set
                                       // bit 6 (also need to change readDateDs1307)
       Wire.send(decToBcd(dow));
       Wire.send(decToBcd(day));
       Wire.send(decToBcd(month));
       Wire.send(decToBcd(year));
       Wire.endTransmission();
       Prompt();
    }*/
  }
}
}

void Rbprompt() {
  Serial.println("R?");
}

void Redboot() {
  boolean rbex = false;
  boolean torchs = false;
  Serial.println(RBVERSION);
  while(!rbex) {
    if (Serial.available() > 0) {
      int rbc = Serial.read();
      switch (rbc) {
        case 'b':
          rbex = true;
          Serial.println("WAIT BOOT");
          break;
          
        case 'f':
          Serial.println(FreeRam());
          Rbprompt();
          break;
          
        case 's':
          Serial.println(analogRead(LEFT_IRS_PIN));
          Serial.println(analogRead(RIGHT_IRS_PIN));
          Serial.println(analogRead(PRECISE_IRS_PIN));
          Rbprompt();
          break;
          
        case 't':
          PinParse();
          delay(10);
          digitalWrite(TORCHPIN, torchs);
          torchs = !torchs;
          Rbprompt();
          break;
          
        case 'l':
          int var1;
          int var2;
          boolean var3;
          pinMode(5, OUTPUT);
          pinMode(6, OUTPUT);
          pinMode(8, OUTPUT);
          pinMode(11, OUTPUT);
          pinMode(12, OUTPUT);
          pinMode(13, OUTPUT);
          delay(20);
          Serial.println("WP?");
          while (Serial.available() < 1) {delay (1);}
          var1 = Serial.read();
          switch(var1) {
            case '1':
              var2 = 5;
              break;
            case '2':
              var2 = 6;
              break;
            case '3':
              var2 = 8;
              break;
            case '4':
              var2 = 11;
              break;
            case '5':
              var2 = 12;
              break;
            case '6':
              var2 = 13;
              break;
          }
          Serial.println("WS?");
          while (Serial.available() < 1) {delay (1);}
          var1 = Serial.read();
          switch(var1) {
            case '1':
              var3 = true;
              break;
            default:
              var3 = false;
              break;
          }
          digitalWrite(var2, var3);
          Rbprompt();
          break;
          
        }
    }
  }
}

void setup(void) {
  pinMode(RBPIN, INPUT);
  Serial.begin(BDRATE);
  if (digitalRead(RBPIN)) Redboot();
  lcd.begin(16, 2);
  lcd.createChar(0, key);
  lcd.createChar(1, degree);
  lcd.clear();
  lcd.setCursor(0, 0);
  Serial.print(OSTYPE);
  Serial.println(DEBUGVERSION);
  Serial.print(FreeRam());
  Serial.println(" B FREE");
  lcd.print("Parsing pins...");
  Dprompt();
  Serial.println("PINS");
  PinParse();
  delay(500);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Att. servos...");
  leftservo.attach(9);
  rightservo.attach(10);
  Dprompt();
  Serial.println("SERVOS");
  delay(500);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Joining I2C...");
  Dprompt();
  Serial.println("I2C");
  Wire.begin();
  delay(500);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Clearing log...");
  Dprompt();
  Serial.println("CLEEPROM");
  for (int i = 0; i < 1024; i++) EEPROM.write(i, 0);
  memaddr = 0;
  Dprompt();
  Serial.println("AUTHSTART");
  PerformAuth();
  delay(750);
  Dprompt();
  Serial.println("AUTHEND");
  lcd.setCursor(3, 0);
  Dprompt();
  Serial.println("VERSION");
  lcd.print(OSTYPE);
  lcd.setCursor(2, 1);
  lcd.print(OSVERSION);
  delay(3000);
  lcd.clear();
}

void loop(void) {
  int sloop = millis();
  delay(100);
  InLoop();
  Serial.println("ISCONSOLE");
  if(Serial.available() > 0) {
    Console();
  }
  lcd.setCursor(0,1);
  lcd.print("                ");
  lcd.setCursor(0,1);
  InLoop();
  Serial.println("LIRS");
  lcd.print("L:");
  lirs = IfIRS(LEFT_IRS_PIN);
  lcd.print(lirs);
  InLoop();
  Serial.println("RIRS");
  lcd.setCursor(5, 1);
  lcd.print("R:");
  rirs = IfIRS(RIGHT_IRS_PIN);
  lcd.print(rirs);
  lcd.setCursor(10, 1);
  InLoop();
  Serial.println("HALL");
  lcd.print("M:");
  IfHall();
  lcd.print(hall);
  //lcd.print("F");
  //Serial.println("DEBUG:LOOP:FILES");
  //lcd.print(lasteaddr / 7);
  //if(lasteaddr >= 32767) {
  //  lcd.write(1);
  //}
  lcd.setCursor(0,0);
  int curmil = millis();
  if(curmil - lastmil1 > TSDELAY) {
    InLoop();
    Serial.print("TEMP:");
    lastmil1 = curmil;
    lcd.print("      ");
    lcd.setCursor(0,0);
    delay(100);
    IfThermo();
    delay(100);
    lcd.print(temp);
    lcd.write(1);
    lcd.print(" ");
    Serial.println(temp);
  }
  curmil = millis();
  if(curmil - loglast > LWDELAY) {
    IfLogRecord();
    loglast = curmil;
  }
  /*curmil = millis();
  if(curmil - lastmil2 > 1000) {
    lastmil2 = curmil;
    Serial.println("DEBUG:LOOP:WREVENT");
    MemWrEvent();
  }*/
  lcd.setCursor(6, 0);
  InLoop();
  Serial.println("PIRS");
  lcd.print(" P:");
  pirs = IfIRS(PRECISE_IRS_PIN);
  lcd.print(pirs);
  lcd.print("     ");
  InLoop();
  Serial.println("TIME");
  lcd.setCursor(12,0);
  lcd.print("S:");
  lcd.print(millis() / 10000);
  lcd.setCursor(15,0);
 /* Serial.println("DEBUG:LOOP:DIRECTION");
  if(lservobalance > 97 && rservobalance > 97) {
    lcd.print("b");
  } else if(lservobalance < 97 && rservobalance < 97) {
    lcd.print("f");
  } else if(lservobalance < 97) {
    lcd.print("r");
  } else if(rservobalance < 97) {
    lcd.print("l");
  }*/
  //here comes pure robotics
  InLoop();
  Serial.print("ILOOP TIME: ");
  Serial.print(sloop);
  Serial.println(" US");
  InLoop();
  Serial.print("R ST:L: ");
  int lef = IfIRS(LEFT_IRS_PIN);
  Serial.print(lef);
  Serial.print(" R:");
  int rgt = IfIRS(RIGHT_IRS_PIN);
  Serial.println(rgt);
  delay(10);
  lirs = IfIRS(LEFT_IRS_PIN);
  rirs = IfIRS(RIGHT_IRS_PIN);
  if(lirs < LIRRA /*&& lirs > -2*/ && rirs < RIRRA /*&& rirs > -2 */) {
    InLoop();
    Serial.println("R:BW:1");
    IfTruckControl(4, 80);
    delay(1000);
    int dice = random(0, 1);
    switch(dice) {
      case 0:
        IfTruckControl(1, 80);
        break;
      case 1:
        IfTruckControl(2, 80);
        break;
      default:
        IfTruckControl(1, 80);
        break;
    }   
    delay(2000);
    IfTruckControl(0, 0);
    return;
  }
  delay(10);
  /*lirs = IfIRS(LEFT_IRS_PIN);
  rirs = IfIRS(RIGHT_IRS_PIN);
  if(lirs < LIRRA && rirs < RIRRA ) {
    InLoop();
    Serial.println("R:BW:2");
    IfTruckControl(0, 0);
    randomSeed(millis() / 2);
    int dice = random(0, 2);
    IfTruckControl(4, 80);
    delay(1000);
    switch(dice) {
      case 0:
        IfTruckControl(1, 80);
        break;
      case 1:
        IfTruckControl(2, 80);
        break;
    }   
    delay(2000);
    IfTruckControl(0, 0);
    return;
  }*/
  //delay(10);
  lirs = IfIRS(LEFT_IRS_PIN);
  if(lirs < LIRRA/* && lirs > -3*/) {
    InLoop();
    Serial.println("R:LIRS:1");
    IfTruckControl(2, 80);
    delay(1000);
    IfTruckControl(0, 0);
    return;
  }
  delay(10);
  /*lirs = IfIRS(LEFT_IRS_PIN);
  if(lirs < -2) {
    InLoop();
    Serial.println("R:LIRS:2");
    IfTruckControl(2, 80);
    delay(2000);
    IfTruckControl(0, 0);
    return;
  }
  delay(10);*/
  rirs = IfIRS(RIGHT_IRS_PIN);
  if(rirs < RIRRA/* && rirs > -3*/) {
    InLoop();
    Serial.println("R:RIRS:1");
    IfTruckControl(1, 80);
    delay(1000);
    IfTruckControl(0, 0);
    return;
  }
  delay(10);
  /*rirs = IfIRS(RIGHT_IRS_PIN);
  if(rirs < -2) {
    InLoop();
    Serial.println("R:RIRS:2");
    IfTruckControl(1, 80);
    delay(2000);
    IfTruckControl(0, 0);
    return;
  }*/
  IfTruckControl(3, 80);
  InLoop();
  Serial.println("R:DEF");
  delay(1000);
  IfTruckControl(0, 0);
}




